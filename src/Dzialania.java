
public class Dzialania {

	public static void main(String[] args) {
		System.out.print("2+3 : ");
		int a = 2+3;
		System.out.println(a);
		
		System.out.print("5/2 : ");
		System.out.println(5/2);

		System.out.print("'A'+2 : ");
		char a2 = 'A' + 2;
		System.out.println(a2);
			
		System.out.print("\"a\" + \"b\" : ");
		System.out.println("a" + "b");
		
		System.out.print("'a' + 'b' : ");
		System.out.println('a' + 'b');
		
		System.out.print("\"a\" + 'b' : ");
		System.out.println("a" + 'b');
		
		System.out.print("\"a\" + 'b' + 3 : ");
		System.out.println("a" + 'b' + 3);
		
		System.out.print("'b' + 3 + \"a\": ");
		String s = 'b' + 3 + "a";
		System.out.println(s);
		

	}

}
