
public class DzialaniaNaTablicach {

	public static void main(String[] args) {

		int[] liczby = {1,3,2,1,3,5,2,3};
		suma(liczby);
		iloczyn(liczby);
		silnia();
		sumaCi�gu();
	}

	private static int obliczSumeCiagu(int r, int n, int a0) {
		int element = a0;
		int suma = 0;
		
		for (int i = 0 ; i < n ; i++) {
			suma += element;
			element+=r;
		}
		return suma;
	}
	
	private static void sumaCi�gu() {
		System.out.println("Suma ci�gu " + obliczSumeCiagu(5, 3, 1));
	}

	private static int obliczSilnie(int n) {
		int silnia=1;
		for (int i=2;i<=n;i++){
			silnia=silnia*i; // silnia *= i;
		}
		return silnia;
	}
	
	private static void silnia() {
		System.out.println("Silnia");
		System.out.println("5!="+obliczSilnie(5));
	}

	private static void iloczyn(int[] liczby) {
		int iloczyn = 1;
		//iloczyn
		for(int liczba : liczby){
			iloczyn*= liczba;
		}
		System.out.println("Iloczyn : " + iloczyn);
	}

	private static void suma(int[] liczby) {
		int suma = 0;
		//Sumowanie w p�tli...
		
		for (int liczba: liczby){
			suma += liczba;
		}
		
		System.out.println("Suma : " + suma);
	}

}
