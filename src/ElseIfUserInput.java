import java.util.Scanner;

public class ElseIfUserInput {

	public static void main(String[] args) {
		Scanner consoleScanner = new Scanner(System.in);

		System.out.println("Podaj liczb�");
		int x = consoleScanner.nextInt();

		if (x % 3 == 0) {
			System.out.println("Ta liczba jest podzielna przez 3");
		} else if (x % 2 == 0) {
			System.out.println("Ta liczba nie jest podzielna przez 3 , ale jest parzysta");
		} else {
			System.out.println("Ta liczba nie jest podzielna przez 3 i jest nieparzysta");

		}

		consoleScanner.close();

	}
}
