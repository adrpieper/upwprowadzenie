
public class GeoMath {

	public static double squareArea(double a) {
		return a*a;
	}
	
	public static double cubeArea(double a){
		return squareArea(a)*6;
	}
	
	public static double circleArea(double r){
		return Math.PI*r*r;
	}
	
	public static double cylinderVolume(double r, double h){
		return circleArea(r)*h;
	}
	
	public static double coneVolume(double r, double h){
		return cylinderVolume(r,h)/3;
	}
	
	public static double cubeVolume(double a){
		return squareArea(a)*a;
	}
	
	public static double pyramidVolume(double a, double h){
		return squareArea(a)*h/3;
	}
	
}
