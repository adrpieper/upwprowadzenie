import org.junit.Test;

public class GeoMathTest {

	@Test
	public void squareAreaTest() {
		assert GeoMath.squareArea(2.0) == 4.0;
		assert GeoMath.squareArea(1.5) == 2.25;
		assert GeoMath.squareArea(0) == 0;
	}
	
	@Test
	public void cubeAreaTest() {
		assert GeoMath.cubeArea(2.0) == 24.0;
		assert GeoMath.cubeArea(0.5) == 1.5;
		assert GeoMath.cubeArea(0) == 0;
	}
	
	@Test
	public void circleAreaTest() {
		assert GeoMath.circleArea(1) > 3.1415;
		assert GeoMath.circleArea(1) < 3.1416;
		assert GeoMath.circleArea(2) > 4*3.1415;
		assert GeoMath.circleArea(2) < 4*3.1416;
	}
	
    @Test
    public void cylinderTest() {
        assert GeoMath.cylinderVolume(2, 4) > 50.26;
        assert GeoMath.cylinderVolume(2, 4) < 50.27;
    }
    
    @Test
    public void coneTest() {
        assert GeoMath.coneVolume(2, 5) > 20.94;
        assert GeoMath.coneVolume(2, 5) < 20.95;
    }

    
    public void coneVolumeTest() {
        assert GeoMath.coneVolume(1, 3) > 3.1415;
        assert GeoMath.coneVolume(1, 3) > 3.1416;
        assert GeoMath.coneVolume(2, 3) > 4*3.1415;
        assert GeoMath.coneVolume(2, 3) > 4*3.1416;
    }
}
