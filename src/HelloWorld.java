
public class HelloWorld {

	public static void main(String[] args) {
		byte b = 10;
		System.out.println("byte : " + b);
		short s = 10;
		System.out.println("short : " + s);
		int i = 10;
		System.out.println("int : " + i);
		long l = 10000000000L;
		System.out.println("long : " + l);
		float f = (float) 10.5;
		System.out.println("float : " + f);
		double d = 10.5;
		System.out.println("double : " + d);
		char c = 'a';
		System.out.println("char : " + c);
		boolean bool = true;
		System.out.println("boolean : " + bool);
		

		System.out.println((char) 98);
		System.out.println((int) 'a');
		
		String text; // Deklaracja zmiennej
		text = "Hello World! 1"; // Inicjalizacja zmiennej
		text = "Hello World! 2";
		System.out.println(text); // Wypisanie warto�ci zmiennej na ekran.
		text = "Hello World! 3";
		text = "Hello World! 4";
		text = "Hello World! 5";
		System.out.println(text);
		

		int a1 = 1; // Deklaracja i inicjalizacja zmiennej
		int a2 = 2;
		int a3 = 3;
		System.out.println("a : " + a1 + a2 + a3);
		

	}

}
