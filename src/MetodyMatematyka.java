
public class MetodyMatematyka {

	public static void main(String[] args) {
		suma(5,2);
		System.out.println("Iloczyn wynosi : " + iloczyn(4,7));
		
		int i = iloczyn(2, 3) + iloczyn(4, 2);
	//  Tak nie mo�na 
	//  int s = suma(2,3) + suma(4,2); 

		System.out.println("Mniejsza spo�r�d 3 i 8 : " + mniejsza(3, 8));
		System.out.println("Mniejsza spo�r�d 1, 3 i 5 : " + mniejsza(1, 3, 5));
		System.out.println("Mniejsza spo�r�d 4, 2 i 8 : " + mniejsza(4, 2, 8));
	
		int[] tab = {1 ,2 ,3 ,4 ,5};
		int szukana = 4;
		
		System.out.println("Pozycja szukanej liczby to: " +szukaj(tab, szukana));
	}

	private static void suma(int a, int b) {
		int suma = a + b;
		System.out.println("Suma wynosi : " + suma);
	}
	
	public static int iloczyn(int a, int b) {
		int iloczynDwoch = a * b;
		return iloczynDwoch;
		
	}
	
	public static int mniejsza(int a, int b) {
		if (a<=b) {
			return a;
		}else {
			return b;
		}
		// to samo :  return a <= b ? a : b;
	}

	public static int mniejsza(int a, int b, int c) {
		//return (a lub b)
		int ab = mniejsza(a, b);
		int abc = mniejsza(ab, c);
		return abc;
	}	
	
	public static int szukaj(int[] liczby, int liczba){
		
		for (int i =0; i<liczby.length; i++)
		{
			if (liczba == liczby[i]){
				
				return i;
			}
		}
		return -1;
		
	}
}
