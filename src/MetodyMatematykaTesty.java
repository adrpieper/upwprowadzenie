import org.junit.Test;

public class MetodyMatematykaTesty {
	
	//Publiczna, void , dowolna nazwa, brak parametrów
	
	@Test
	public void testIloczynu() {

		assert MetodyMatematyka.iloczyn(1, 1) == 1;
		assert MetodyMatematyka.iloczyn(0, 10) == 0;
		assert MetodyMatematyka.iloczyn(1, 10) == 10;
		assert MetodyMatematyka.iloczyn(3, 4) == 12;
	}
	
	@Test
	public void testSzukaj() {

		int[] liczby = {1,2,3,5,3};
		assert MetodyMatematyka.szukaj(liczby, 3) == 2;
		assert MetodyMatematyka.szukaj(liczby, 4) == -1;
		assert MetodyMatematyka.szukaj(liczby, 5) == 3;
	}
	
	@Test
	public void testMniejsza2() {

		assert MetodyMatematyka.mniejsza(1, 3) == 1;
		assert MetodyMatematyka.mniejsza(2, 1) == 1;
		assert MetodyMatematyka.mniejsza(-20, 20) == -20;
	}
	
	@Test
	public void testMniejsza3() {

		assert MetodyMatematyka.mniejsza(1, 2, 3) == 1;
		assert MetodyMatematyka.mniejsza(2, 1, 3) == 1;
		assert MetodyMatematyka.mniejsza(3, 2, 1) == 1;
		assert MetodyMatematyka.mniejsza(1, 2, 2) == 1;
		assert MetodyMatematyka.mniejsza(1, 1, 2) == 1;
	}
	
}