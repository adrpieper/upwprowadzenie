import org.junit.validator.PublicClassValidator;

public class MyMath {
	
	public static int max(int a, int b) {
		if (a>b){
			return a;
		}
		else {
			return b;
		}
	}
	
	public static double max(double a, double b) {
		if (a>b){
			return a;
		}
		else {
			return b;
		}
	}
	
	public static int abs (int a){
		if (a>0){
			return a;
		}
		else{
			return -a;
		}
		
	}
	public static double abs (double a){
		if (a>0){
			return a;
		}
		else{
			return -a;
		}
	}
	public static long pow(int a, int n) {
		long wynik = 1;
		
		for(int i=0; i<n; i++){
			wynik=a*wynik; // wynik*=a;
		}
		
		return wynik;
	}

	public static long pow(long a, int n) {
		long wynik = 1;
		
		for(int i=0; i<n; i++){
			wynik=a*wynik; // wynik*=a;
		}
		
		return wynik;
	}

	public double pow(float a, int n) {
		double wynik = 1;
		
		for(int i=0; i<n; i++){
			wynik=a*wynik; // wynik*=a;
		}
		
		return wynik;
	}

	public double pow(double a, int n) {
		double wynik = 1;
		
		for(int i=0; i<n; i++){
			wynik=a*wynik; // wynik*=a;
		}
		
		return wynik;
	}
	
}
