import org.junit.Test;

public class MyMathTest {

	@Test
	public void absIntegerTest() {
		assert MyMath.abs(4) == 4;
		assert MyMath.abs(-4) == 4;
		assert MyMath.abs(0) == 0;
	}
	
	@Test
	public void absDoubleTest() {
		assert MyMath.abs(4.5) == 4.5;
		assert MyMath.abs(-4.5) == 4.5;
		assert MyMath.abs(0.0) == 0.0;
	}
	
	@Test
	public void powIntegerTest() {
		assert MyMath.pow(4,3) == 64;
	}
}
