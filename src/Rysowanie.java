
public class Rysowanie {

	public static void main(String[] args) {
		rysujTrojkat(5,10);
	}

	private static void rysujProstokat(int w, int h) {
		for (int i = 1; i <= h; i++) {
            rysujLinie(w);
        }
	}

	private static void rysujLinie(int dlugoscLinii) {
		for (int j = 1; j <= dlugoscLinii; j++) {
		    System.out.print("*");
		}
		System.out.println();
	}

	private static void rysujTrojkat(int a) {
		for (int i = 1; i <= a; i++) {
            rysujLinie(i);
        }
	}
	

	private static void rysujTrojkat(int a, int b) {
		for (int i = 1; i <= a; i++) {
            rysujLinie(i);
        }
	}
	
}
