
public class Tablice {

	public static void main(String[] args) {
		 // prostePrzyklady();
		
		int[] liczby = {1,3,5,10};
		System.out.println("Reczne wypisanie: ");
		System.out.println(liczby[0]);
		System.out.println(liczby[1]);
		System.out.println(liczby[2]);
		System.out.println(liczby[3]);

		System.out.println("P�tla for: ");
		for(int i=0; i<liczby.length; i++){
			System.out.println(liczby[i]);
		}

		System.out.println("Reczne for od ty�u: ");
		for(int i=liczby.length-1; i>=0; i--) {
			System.out.println(liczby[i]);
		}
		

		System.out.println("P�tla forEach: ");
		for(int liczba : liczby) {
			System.out.println(liczba);
		}

	}
	
		

	private static void prostePrzyklady() {
		int[] liczby = {2,3};

		System.out.println(liczby[0]);
		System.out.println(liczby[1]);
		
		int [] liczby100 = new int[100]; // Tablica stu zer
		System.out.println(liczby100[99]); // maksymalny index
		
		boolean[] boole = new boolean[2];
		System.out.println(boole[0]);
		String[] slowa = new String[10];
		System.out.println(slowa[3]);
		char[] znaki = new char[3];
		System.out.println(znaki[2]);
		
		
		System.out.println(liczby100.length);
	}

}
